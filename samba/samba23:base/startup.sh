#!/bin/bash
# @edt ASIX-M06
# ------------------------


# Creació de xixa per als shares
# Share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p  /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /etc/os-release  /var/lib/samba/privat/.

# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf
