SCHEMA



ENUNCIADO 


    Pujar-la al git

    Pujar-la al dockerhub

    Generar els README.md apropiats

    Crear un schema amb:

        Un nou objecte STRUCTURAL

        Un nou objecte AUXILIARU

        Cada objecte ha de tenir almenys 3 nous atributs.

        Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i binary per contenir documents pdf.

    Crear una nova ou anomenada practica.

    Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass definits en l’schema. 

        Assegurar-se de omplir amb dades reals la foto i el pdf.

    Eliminar del slapd.conf tots els schema que no facin falta, deixar només els imprescindibles

    Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el pdf.


CREAMOS EL SCHEMA QUE EN MI CASO LE PONGO EL NOMBRE DE practica.schema


    attributetype (1.1.2.1.1 NAME 'x-nombre'
    DESC 'Nombre del llibre.'
    EQUALITY caseIgnoreMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
    SINGLE-VALUE 
    )

    attributetype ( 1.1.2.1.3 NAME 'x-llibre'
    DESC 'LLibre.'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
    )

    attributetype (1.1.2.2.3 NAME 'x-disponible'
    DESC 'Disponible true/false.'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
    SINGLE-VALUE )


    attributetype (1.1.2.2.4 NAME 'x-descatalogs'
    DESC 'Descatalogs true/false.'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
    SINGLE-VALUE 
    )


    attributetype (1.1.2.1.2 NAME 'x-portada'
    DESC 'Portada en foto'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.28
    SINGLE-VALUE 
    )
#x-----------------------------------------

    objectClass (1.1.2.1.1 NAME 'x-biblioteca'
    DESC 'LLibres de la biblioteca.'
    SUP TOP
    STRUCTURAL
    MUST ( x-nombre $ x-llibre )
    MAY ( x-disponible $ x-portada $ x-descatalogs )
    )
  
    objectClass (1.1.2.2.2 NAME 'x-descatalogats'
    DESC 'Descatalogats.'
    SUP TOP
    AUXILIARY
    MUST ( x-descatalogs $ x-nombre )
    MAY ( x-disponible $ x-portada $ x-llibre )
    )

AFEGIM ELS USUARIS I LA OU  EN EL MEU CAS DIRECTAMENT A EDT-ORG.ldif

    dn: ou=practica,dc=edt,dc=org
    ou: practica
    description: Practica schema
    objectclass: organizationalunit

    dn: x-nombre=Pep Guardiola. La metamorfosis.,ou=practica,dc=edt,dc=org
    objectClass: x-biblioteca
    objectClass: x-descatalogats
    x-portada:< file:/opt/docker/PEP.jpeg
    x-llibre:< file:/opt/docker/PEP.pdf
    x-disponible: TRUE
    x-descatalogs: FALSE

    dn: x-nombre=¿Qué esconde tu rival?,ou=practica,dc=edt,dc=org
    objectClass: x-biblioteca
    objectClass: x-descatalogats
    x-portada:< file:/opt/docker/esconde.jpeg
    x-llibre:< file:/opt/docker/ESCONDE.pdf
    x-disponible: FALSE
    x-descatalogs: TRUE

DESPRES DE FER TOT AIXO GENEREM LA IMATGE 

    docker build -t oscarct06/ldap03:schema .  
Y ENGEMEM EL DOCKER 

    docker run --rm --name cont1 -h ldap.edt.org -p 389:389 --net edtasix -d oscarct06/ldap03:schema
    


