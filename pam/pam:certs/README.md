Arrancar amb la segun comanda
    
    docker run --rm --name pam.edt.org  -h pam.edt.org  --net 2hisx --privileged -d oscarct06/pam24escola:latest



Exemple de muntatge WebDav al pam_mount.conf.xml


        <volume 
        uid="*" 
        fstype="davfs" 
        path="https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/%(USER)" 
        mountpoint="~/%(USER)dav"
        options="username=%(USER),uid=%(USERUID),gid=%(USERGID),file_mode=0770,dir_mode=0770"
        /
