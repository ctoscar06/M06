#! /usr/sbin/phyhon
#-*- coding:utf-8-*-
#
#-------------------------------------
import sys
MAX=5
fileIn=sys.stdin
if len(sys.argv)==2:
    fileIn=open(sys.argv[1], "r")
counter=0   
for line in fileIn:
    counter+=1
    print (line,end="")
    if counter==MAX:break
fileIn.close()
exit(0)
