import sys,argparse
parser = argparse.ArgumentParser(\
    description="Head args",\
    prog="04b-head-choices.py",\
    epilog="loco")

parser.add_argument("-n", "--nlin" , choices= [5, 10 ,15],
    type=int, metavar="nlin" , help="Numero de linias", dest="nlin" ,
    default=10)

parser.add_argument("-f", "--file", type=str,
    metavar="file" , help="File especifico" , dest="filelist" , 
    action="append")
args=parser.parse_args()
print(args)
#----------------------------
MAX=args.nlin

def headFile(fitxer, max):
    fileIn=open(args.fitxer, "r")

    cont=0
    for line in fileIn:
        cont += 1
    print(line, end="")
    if cont == MAX: break

    fileIn.close

if args.fileList:
    for file in args.filelist:
        if args.verbose:
        print("\n", args.fileName, 40*"-")
    headFile(file)
exit(0)