# /user/bin/python3
#-*- coding: utf-8-*-
# @ Abril 2024
#ECHO SERVER
#-------------------------------------------------------------
import sys,socket
HOST=''
PORT=5001
s= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()
print("Conn:", type (conn), conn)
print("Connected by:",addr)
while True:
    data = conn.recv(1024)
    if not data: break #No significa que no hi ha dades
    conn.send(data)
    print(data)
    conn.close()
    sys.exit(0)