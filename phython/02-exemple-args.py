#! /usr/sbin/phyhon
#-*- coding:utf-8-*-
#
#-------------------------------------
import argparse


parser = argparse.ArgumentParser(\
    description="programa d'exemple",\
    prog="02-argument.py",\
    epilog="hasta luego lucas!" )
parser.add_argument("-n","--nom",type=str,\
help="nom usuari")
parser.add_argument("-e","--edat",type=str,\
dest="userEdat", help="edat a processar",\
metavar="edat")
args=parser.parse_args()
print (args)
print(args.userEdat,args.nom)
exit(0)