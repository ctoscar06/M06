#! /usr/sbin/phyhon
#-*- coding:utf-8-*-
#head [-n nlin] [-f file]
#default=10,file o stdin
#-------------------------------------
import sys, argparse


parser = argparse.ArgumentParser(\
    description="programa d'exemple",\
    prog="03-head-args.py",\
    epilog="loco" )
parser.add_argument("-n","--nlin",type=int,\
help="numero linies ")
parser.add_argument("-e","--edat",type=str,\
dest="userEdat", help="edat a processar",\
metavar="edat")
args=parser.parse_args()
print (args)
print(args.userEdat,args.nom)
exit(0)